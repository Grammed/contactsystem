# Project: Contacts

###Note:
* You can add contact groups to contacts AND vice-versa.
* logic model and repository packages have been used to separate responsibilities.
* Where crossover between ContactGroup logic and Contact logic is necessary, the responibility
has been kept separate: ContactGroupLogic only-> ContactGroupRepository and ContactLogid only->
ContactRepository.  
  
####Issues/Todo:
1330 2805

1.Print tool not being used

2.Editing functionality not available (Optional)

3.Search functionality not available (Optional)

4. Todo --> Use entity graph to define the relationship between contacts and
contact groups with the fetching of data.
   
5. Todo --> Make use of validation in this application.

0950 2905

1. Use Set/hashset instead of list for elements in @ManyToMany joins unique..?


# To create jar
`mvn package`

# To run project
1. First compile, test or create jar. `mvn compile`|`mvn test`|`mvn package`
2. Run
    - run source `mvn exec:java -Dexec.mainClass=uk.co.gram.App`
    - run jar `java -jar target/Contacts.jar`

> # This can be done in one command
> - Compile & run source: `mvn compile exec:java -Dexec.mainClass=uk.co.gram.App`
> - package & run jar: `mvn package && java -jar target/Contacts.jar`

# To create documentation & site
- Documentation: `mvn javadoc:javadoc`
- Site: `mvn site`  
