package uk.co.gram.repositories;

import uk.co.gram.logic.ContactEditingLogic;
import uk.co.gram.logic.ContactLogic;
import uk.co.gram.models.*;
import uk.co.gram.tools.AdjustedConsoleInputTool;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import static uk.co.gram.EntityManagerSingleton.getEm;

public class ContactRepositoryImp implements ContactRepository {


@Override
public Contact addOrUpdateContact(Contact contact) {
	getEm().getTransaction().begin();
	getEm().persist(contact);
	getEm().getTransaction().commit();

	return contact;
}


@Override
public void editField(Contact contact, Integer choice) {
	AdjustedConsoleInputTool adj = new AdjustedConsoleInputTool();
	ContactEditingLogic contactEditingLogic = new ContactEditingLogic();
	ContactLogic contactLogic = new ContactLogic();
	String currentValue;
	LocalDate currentDateOfBirth;
	getEm().getTransaction().begin();
	String update;
	LocalDate dateUpdate;
	boolean ongoingEdit;

	switch (choice) {

		case 1:
			currentValue = contact.getFirstName();
			do {
				update = adj.getUserString("Current first name: " + currentValue + "\n    enter new name: ");
				ongoingEdit = adj.askUserEnterOrTrue(currentValue + " --> " + update +
				"\ncorrect -> enter\ntry again -> any character + enter");
			} while (ongoingEdit);
			contact.setFirstName(update);
			break;
		case 2:
			currentValue = contact.getLastName();
			do {
				update = adj.getUserString("Current last name: " + currentValue + "\n    enter new name: ");
				ongoingEdit = adj.askUserEnterOrTrue(currentValue + " --> " + update +
				"\ncorrect -> enter\ntry again -> any character + enter");
			} while (ongoingEdit);
			contact.setLastName(update);
			break;
		case 3:
			Address updatedAddress = contactLogic.setContactAddress();
			contact.setAddress(updatedAddress);
			break;
		case 4:
			currentDateOfBirth = contact.getDob();
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			do {
				System.out.println("Enter new date of birth date:");
				dateUpdate = LocalDate.parse(adj.getUserSimpleDate(), dateTimeFormatter);
				ongoingEdit = adj.askUserEnterOrTrue(currentDateOfBirth + " --> " + dateUpdate +
				"\ncorrect -> enter\ntry again -> any character + enter");
			} while (ongoingEdit);
			contact.setDob(dateUpdate);
			break;
		case 5:
			List<String> retrievedEmails = contact.getEmails();
			if (retrievedEmails.isEmpty()) {
				System.out.println("contact has no email on record yet ->");
				retrievedEmails = contactLogic.addEmails();
			}
			else
				retrievedEmails = contactEditingLogic.editEmails(retrievedEmails, adj);
			contact.setEmails(retrievedEmails);
			break;
		case 6:
			Map<PhoneType, Phone> retrievedPhoneMap = contact.getPhoneNumbers();
			retrievedPhoneMap = contactLogic.setNewContactNumbers(retrievedPhoneMap);
			contact.setPhoneNumbers(retrievedPhoneMap);
			break;
		default:
			System.out.println("Integer input for editContactGroup is out of range.");
	}
	getEm().getTransaction().commit();
}


@Override
public Contact retrieveSingleContact(Integer id) {
	TypedQuery query = getEm().createQuery("SELECT c FROM Contact c WHERE id = :id", Contact.class);
	query.setParameter("id", id);
	return (Contact) query.getSingleResult();

}


public Contact findContactById(Integer id) throws Exception {
	Contact retrievedContact = getEm().find(Contact.class, id);
	if (retrievedContact == null) {
		throw new Exception("No Contact with id " + id + "found");
	} else {
		System.out.println("\nContact found\n");
	}
	return retrievedContact;
}


@Override
public void deleteSingleContact(Integer id) throws Exception {
	//confirmed that contact removal cascades to Groups contact list.
	Contact retrievedContact = findContactById(id);
	getEm().getTransaction().begin();
	getEm().remove(retrievedContact);
	getEm().getTransaction().commit();
}


@Override
public List<Contact> retrieveAllContacts() {
	return getEm().createQuery("SELECT c FROM Contact c", Contact.class).getResultList();
}


@Override
public void addGroupToContact(Contact contact, ContactGroup contactGroup) {
	List<ContactGroup> addTo = contact.getContactGroups();
	if (addTo.contains(contactGroup))
		System.out.println("\nContact is already in that group.");
	else {
		addTo.add(contactGroup);
		getEm().getTransaction().begin();
		contact.setContactGroups(addTo);
		getEm().getTransaction().commit();
	}
}


@Override
public void removeGroupFromContact(Contact contact, ContactGroup contactGroup) {
	List<ContactGroup> removeFrom = contact.getContactGroups();
	removeFrom.remove(contactGroup);
	getEm().getTransaction().begin();
	contact.setContactGroups(removeFrom);
	getEm().getTransaction().commit();
}


public void removeGroupFromContact(int contactId, ContactGroup contactGroup) {
	Contact contact = retrieveSingleContact(contactId);
	removeGroupFromContact(contact, contactGroup);
}
}
