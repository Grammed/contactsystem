package uk.co.gram.repositories;


import uk.co.gram.models.ContactGroup;

import java.util.List;

public interface ContactGroupRepository {
	ContactGroup addNewGroup(ContactGroup contactGroup);
	ContactGroup retrieveSingleGroup(Integer id);
	List<ContactGroup> retrieveAllGroups();
	void deleteSingleContactGroup(Integer id);
	void editField(ContactGroup retrievedGroup, Integer choice);
}
