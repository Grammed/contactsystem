package uk.co.gram.repositories;

import uk.co.gram.models.Contact;
import uk.co.gram.models.ContactGroup;
import uk.co.gram.tools.AdjustedConsoleInputTool;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static uk.co.gram.EntityManagerSingleton.getEm;

public class ContactGroupRepositoryImp implements ContactGroupRepository {


@Override
public ContactGroup addNewGroup(ContactGroup contactGroup) {
	getEm().getTransaction().begin();
	getEm().persist(contactGroup);
	getEm().getTransaction().commit();
	return contactGroup;
}


@Override
public void editField(ContactGroup group, Integer choice) {

	AdjustedConsoleInputTool adj = new AdjustedConsoleInputTool();
	String currentValue;
	LocalDate currentStartDate;
	getEm().getTransaction().begin();
	String update;
	LocalDate dateUpdate;
	boolean ongoingEdit;

	switch (choice) {
		case 1:
			currentValue = group.getName();
			do {
				update = adj.getUserString("Current group name: " + currentValue + "\n    enter new name: ");
				ongoingEdit = adj.askUserEnterOrTrue(currentValue + " --> " + update +
				"\ncorrect -> enter\ntry again -> any character + enter");
			} while (ongoingEdit);
			// todo UPDATE relationship is not doubled in database could be where it prints.. BUG on editing name the contact ends up with edited name in its group list twice -> create new group// add contact to it during its creation // edit group.
			group.setName(update);
			break;
		case 2:
			currentStartDate = group.getStartDate();
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			do {
				System.out.println("Enter new start date:");
				dateUpdate = LocalDate.parse(adj.getUserSimpleDate(), dateTimeFormatter);
				ongoingEdit = adj.askUserEnterOrTrue(currentStartDate + " --> " + dateUpdate +
				"\ncorrect -> enter\ntry again -> any character + enter");
			} while (ongoingEdit);
			group.setStartDate(dateUpdate);
			break;
		case 3:
			currentValue = group.getDescription();
			do {
				update = adj.getUserString("Current description : " + currentValue + "\n    enter new description: ");
				ongoingEdit = adj.askUserEnterOrTrue(currentValue + "\n --> \n" + update +
				"\ncorrect -> enter\ntry again -> any character + enter");
			} while (ongoingEdit);
			group.setDescription(update);
			break;
		default:
			System.out.println("Integer input for editContactGroup is out of range.");
	}
	getEm().getTransaction().commit();
}


@Override
public ContactGroup retrieveSingleGroup(Integer id) {
	TypedQuery query = getEm().createQuery("SELECT cg FROM ContactGroup cg WHERE id = :id", ContactGroup.class);
	query.setParameter("id", id);
	return (ContactGroup) query.getSingleResult();
}


@Override
public List<ContactGroup> retrieveAllGroups() {
	return getEm().createQuery("SELECT cg FROM ContactGroup cg", ContactGroup.class).getResultList();
}


@Override
public void deleteSingleContactGroup(Integer id) {
	AdjustedConsoleInputTool adjustedConsoleInputTool = new AdjustedConsoleInputTool();
	boolean stillDelete;
	ContactGroup retrievedContactGroup = retrieveSingleGroup(id);
	List<Contact> condemnedContacts = retrievedContactGroup.getContacts();

	if (!condemnedContacts.isEmpty()) {
		for (Contact c : condemnedContacts)
			System.out.println(c.getId() + " " + c.getFirstName() + " " + c.getLastName());
	}
	stillDelete = adjustedConsoleInputTool.askUserEnterOrTrue("\nGroup contains the above contacts!\n" +
	"back to previous menu -> enter\ncomplete deletion -> any character + enter");

	//todo is this still the case? THIS should be the only place where separation rule is broken UPSTREAM necessary.
	if (stillDelete) {
		if (!condemnedContacts.isEmpty()) {
			ContactRepositoryImp contactRepositoryImp = new ContactRepositoryImp();
			for (Contact c : condemnedContacts) {
				contactRepositoryImp.removeGroupFromContact(c.getId(), retrievedContactGroup);
			}
		}
		getEm().getTransaction().begin();
		getEm().remove(retrievedContactGroup);
		getEm().getTransaction().commit();
	}
}


// todo delete not used this downstream change does not matter when removing a group
private void clearGroupsContacts(ContactGroup retrievedContactGroup) {

	List<Contact> empty = new ArrayList<>();
	getEm().getTransaction().begin();
	retrievedContactGroup.setContacts(empty);
	getEm().getTransaction().commit();

	if (retrievedContactGroup.getContacts().isEmpty())
		System.out.println("\nGroups contacts removed!");
}
}
