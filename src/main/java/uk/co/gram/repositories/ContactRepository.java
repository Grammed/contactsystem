package uk.co.gram.repositories;

import uk.co.gram.models.Contact;
import uk.co.gram.models.ContactGroup;
import java.util.List;

public interface ContactRepository {
	Contact addOrUpdateContact(Contact contact);
	Contact retrieveSingleContact(Integer id);
	void deleteSingleContact(Integer id) throws Exception;
	List<Contact> retrieveAllContacts();
	void addGroupToContact(Contact contact, ContactGroup contactGroup);
	void removeGroupFromContact(Contact contact, ContactGroup contactGroup);
	void editField(Contact retrievedContact, Integer choice);
}
