package uk.co.gram.models;

import javax.persistence.Embeddable;

@Embeddable
public class Phone {
	private String number;

public String getNumber() {
	return number;
}

public void setNumber(String number) {
	this.number = number;
}

@Override
public String toString() {
	return number;
}
}
