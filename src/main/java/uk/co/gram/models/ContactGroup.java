package uk.co.gram.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//Now two separate entity graphs can be named in fetch and load, nothing is in the default.
// EntityGraph<ContactGroup> eg = em.getEntityGraph("previewContactGroupEntityGraph");
@NamedEntityGraph(name="previewContactGroupEntityGraph", attributeNodes= {
@NamedAttributeNode("name"),
@NamedAttributeNode("startDate")
})
@NamedEntityGraph(name="fullContactGroupEntityGraph", attributeNodes= {
@NamedAttributeNode("name"),
@NamedAttributeNode("startDate"),
@NamedAttributeNode("description"),
@NamedAttributeNode("contacts")
})
@Entity
public class ContactGroup {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private LocalDate startDate;
	private String description;
	@ManyToMany(mappedBy = "contactGroups", cascade={CascadeType.PERSIST}, fetch = FetchType.LAZY)
	private List<Contact> contacts = new ArrayList<>();

public Integer getId() {
	return id;
}

public void setId(Integer id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public LocalDate getStartDate() {
	return startDate;
}

public void setStartDate(LocalDate startDate) {
	this.startDate = startDate;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public List<Contact> getContacts() {
	return contacts;
}

public void setContacts(List<Contact> contacts) {
	this.contacts = contacts;
}

@Override
public String toString() {
	return
	"\n\nid = " + id +
	"\n1. name = " + name +
	"\n2. startDate = " + startDate +
	"\n3. description = " + description +
	"\ncontains " + contacts.size() + " contacts";
}

@Override
public boolean equals(Object o) {
	if (this == o) return true;
	if (o == null || getClass() != o.getClass()) return false;
	ContactGroup that = (ContactGroup) o;
	return id.equals(that.id) && name.equals(that.name) && startDate.equals(that.startDate) && description.equals(that.description) && Objects.equals(contacts, that.contacts);
}

@Override
public int hashCode() {
	return Objects.hash(id, name, startDate, description, contacts);
}
}
