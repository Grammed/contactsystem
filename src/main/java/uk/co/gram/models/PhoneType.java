package uk.co.gram.models;

public enum PhoneType {
	WORK,
	HOME,
	OTHER
}
