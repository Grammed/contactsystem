package uk.co.gram.models;

import javax.persistence.Basic;
import javax.persistence.Embeddable;

@Embeddable
public class Address {
@Basic(optional = false)
private String street;
@Basic(optional = false)
private String number;
@Basic(optional = false)
private String zipCode;
@Basic(optional = false)
private String city;
private String country;


public Address() {
	/*this.street = "constructroStreet";
	this.number = "constructorNumber";
	this.zipCode = "constructorZip";
	this.city = "constructorCity";
	this.country = "constructorCountry";*/
}

public String getStreet() {
	return street;
}

public void setStreet(String street) {
	this.street = street;
}

public String getNumber() {
	return number;
}

public void setNumber(String number) {
	this.number = number;
}

public String getZipCode() {
	return zipCode;
}

public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

public String getCountry() {
	return country;
}

public void setCountry(String country) {
	this.country = country;
}

@Override
public String toString() {
	return "Address{" +
	"street='" + street + '\'' +
	", number='" + number + '\'' +
	", zipCode='" + zipCode + '\'' +
	", city='" + city + '\'' +
	", country='" + country + '\'' +
	'}';
}
}
