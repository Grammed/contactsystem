package uk.co.gram.models;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
public class Contact {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;
private String firstName;
private String lastName;

@Embedded
/*	@AttributeOverrides({
	@AttributeOverride(name="street", column = @Column(name = "address_street")),
	@AttributeOverride(name="number", column = @Column(name = "address_number")),
	@AttributeOverride(name="zipCode", column = @Column(name = "address_zipcode")),
	@AttributeOverride(name="city", column = @Column(name = "address_city")),
	@AttributeOverride(name="country", column = @Column(name = "address_country")),
	})*/
private Address address = new Address();

private LocalDate dob;
// todo could these lists be private check at end.
@ElementCollection
@Column(name = "emailAddress")
@CollectionTable(name = "Emails", joinColumns = @JoinColumn(name = "contact_id"))
List<String> emails;

@ElementCollection
@MapKeyColumn(name = "phoneType")
@MapKeyEnumerated(EnumType.STRING)
@CollectionTable(name = "PhoneNumber", joinColumns = @JoinColumn(name = "contact_id"))
Map<PhoneType, Phone> phoneNumbers;

@ManyToMany(cascade = {CascadeType.PERSIST}, fetch=FetchType.LAZY )
@JoinTable(name = "Contact_ContactGroup",
joinColumns = @JoinColumn(name = "contact_id"),
inverseJoinColumns = @JoinColumn(name = "group_id"))
private List<ContactGroup> contactGroups = new ArrayList<>();


public Contact(){
	//this.firstName = "testOneFirst";
	//this.lastName = "testOneLast";
	//this.address = address;
	//this.dob = LocalDate.now();
	//this.emails = emails;
	//this.phoneNumbers = phoneNumbers;
	//this.contactGroups = contactGroups;
}

public Integer getId() {
	return id;
}

public void setId(Integer id) {
	this.id = id;
}

public String getFirstName() {
	return firstName;
}

public void setFirstName(String firstName) {
	this.firstName = firstName;
}

public String getLastName() {
	return lastName;
}

public void setLastName(String lastName) {
	this.lastName = lastName;
}

public Address getAddress() {
	return address;
}

public void setAddress(Address address) {
	this.address = address;
}

public LocalDate getDob() {
	return dob;
}

public void setDob(LocalDate dob) {
	this.dob = dob;
}

public List<String> getEmails() {
	return emails;
}

public void setEmails(List<String> emails) {
	this.emails = emails;
}

public Map<PhoneType, Phone> getPhoneNumbers() {
	return phoneNumbers;
}

public void setPhoneNumbers(Map<PhoneType, Phone> phoneNumbers) {
	this.phoneNumbers = phoneNumbers;
}

public List<ContactGroup> getContactGroups() {
	return contactGroups;
}

public void setContactGroups(List<ContactGroup> contactGroups) {
	this.contactGroups = contactGroups;
}

@Override
public String toString() {
	return
	"\n\nid = " + id +
	"\n1. firstName = " + firstName +
	"\n2. lastName = " + lastName +
	"\n3. address = " + address.getCity() + " " + address.getStreet() + " " + address.getNumber() +
	"\n" + address.getCountry() + " "  + address.getZipCode() +
	"\n4  dob = " + dob +
	"\n5. emails = " + emails +
	"\n6. phoneNumbers = " + phoneNumbers +
	"\ncontact groups is in = " + contactGroups.size();
}
}
