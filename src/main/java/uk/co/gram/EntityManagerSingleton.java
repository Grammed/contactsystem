package uk.co.gram;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class EntityManagerSingleton {
    private static EntityManagerFactory emf;
    private static EntityManager em;

    EntityManagerSingleton() {
    }

    public static EntityManager getInstance(){
        if(em == null) {
            emf = Persistence.createEntityManagerFactory("myPersistenceUnit");
            em = emf.createEntityManager();
        }

        return em;
    }

    public static EntityManager getEm(){
        return getInstance();
    }

    public static void close(){
        em.close();
        em = null;
        emf.close();
        emf = null;
    }
}
