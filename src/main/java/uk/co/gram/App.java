package uk.co.gram;

import uk.co.gram.logic.ContactGroupLogic;
import uk.co.gram.logic.ContactLogic;
import uk.co.gram.logic.ContactEditingLogic;
import uk.co.gram.tools.AdjustedConsoleInputTool;

public class App {
   AdjustedConsoleInputTool adjustedCIT = new AdjustedConsoleInputTool();
public static void main(String[] args) throws Exception {
      App app = new App();
      app.menuOne();

   }

   void menuOne() throws Exception {
      ContactLogic contactLogic;
      ContactEditingLogic contactEditingLogic;
      ContactGroupLogic contactGroupLogic;
      boolean runApp = true;

      do {
      int choice = adjustedCIT.askUserInputInteger("Please enter choice\n" +
      "1. Add new contact\n" +
      "2. Add new contact group\n" +
      "3. Add/Remove contact/contact group relationship\n" +
      "4. List contacts/contact groups (and delete)\n" +
      "5. Edit menu\n" +
      "0. Quit", 0, 5);

      switch(choice) {
         case 0:
            runApp = false;
            break;
         case 1:
            contactLogic = new ContactLogic();
            contactLogic.collectNewContactData();
            break;
         case 2:
            contactGroupLogic = new ContactGroupLogic();
            contactGroupLogic.collectNewGroupData();
            break;
         case 3:
            boolean addContactToGroup;
            addContactToGroup = adjustedCIT.askUserEnterOrFalse("\nAdd contact to group? -> enter\nremove contact from group (TBC) -> any character and enter");
            if (addContactToGroup) {
              contactLogic = new ContactLogic();
              contactLogic.addContactToGroup();
            } else {
               contactLogic = new ContactLogic();
               contactLogic.removeContactFromGroup();
            }
            break;
         case 4:
            // todo check there are contacts and groups first.
            boolean viewDeleteContacts;
            viewDeleteContacts = adjustedCIT.askUserEnterOrFalse("\nView and delete contacts? -> enter\n" +
            "view and delete contact groups? -> any character and enter");
            if (viewDeleteContacts) {
               contactLogic = new ContactLogic();
               contactLogic.viewAndDeleteContacts();
            }else {
               contactGroupLogic = new ContactGroupLogic();
               contactGroupLogic.viewAndDeleteContactGroups();
            }
            break;
         case 5:
            boolean editContact;
            // todo check there are contacts and groups first.
            editContact = adjustedCIT.askUserEnterOrFalse("\nedit contacts -> enter\n" +
            "edit contact groups -> any character + enter");
            if (editContact) {
               contactEditingLogic = new ContactEditingLogic();
               contactEditingLogic.chooseContactToEdit();
            }else { //todo refactor to editing logic
               contactGroupLogic = new ContactGroupLogic();
               contactGroupLogic.chooseGroupToEdit();
            }
            System.out.println("Coming soon");
            break;

      }

   }while(runApp);

}



}
