package uk.co.gram.tools;

import java.util.InputMismatchException;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class AdjustedConsoleInputTool {

	public void pressEnterToContinue() {
		System.out.println("Press enter to continue.");
		new Scanner(System.in).nextLine();
	}

	private boolean isLeapYear(int yearToCheck) {

		if (yearToCheck % 4 == 0) {
			if (yearToCheck % 100 == 0) {
				return yearToCheck % 400 == 0;
			} else
				return true;
		} else
			return false;
	}

	private boolean isDayOk(int month, int day, boolean leapYear) {
		if (month == 9 || month == 4 || month == 6 || month == 11)
			if (day > 0 && day < 31) {
				return true;
			}
		if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
			if (day > 0 && day < 32) {
				return true;
			}
		if (leapYear && month == 2) {
			return day > 0 && day < 30;
		} else if (month == 2) {
			return day > 0 && day < 29;
		}
		return false;
	}

	public String checkSimpleEntryDate(String simpleEntryDate) {
		simpleEntryDate.trim();
		if (simpleEntryDate.length() != 10) {
			System.out.println("String @ SimpleEntryDate is not 10 characters long");
		}
		boolean isDayOk = false;
		String year;
		String month;
		String day;

		try {
			String checkString = "";
			for (int i = 0; i < simpleEntryDate.length(); i++) {
				if (simpleEntryDate.charAt(i) == '/')
					checkString = checkString + (i + 1);
			}

			if (checkString.equals("58")) {
				int yearI = parseInt(simpleEntryDate.
				substring(0, simpleEntryDate.indexOf('/')));
				int monthI = parseInt(simpleEntryDate.
				substring(5, simpleEntryDate.indexOf('/', 6)));
				int dayI = parseInt(simpleEntryDate.
				substring(8, 10));

				boolean isLeapYear = isLeapYear(yearI);
				if (monthI > 0 && monthI <= 12)
					isDayOk = isDayOk(monthI, dayI, isLeapYear);
				year = simpleEntryDate.
				substring(0, simpleEntryDate.indexOf('/'));
				month = simpleEntryDate.
				substring(5, simpleEntryDate.indexOf('/', 6));
				day = simpleEntryDate.
				substring(8, 10);
				if (isDayOk)
					return year + "-" + month + "-" + day;
			}
		} catch (Exception ex) {
			System.out.println("Error: @SimpleEntryDateTime");
		}
		return null;
	}

	public String getUserSimpleDate() {
		do {
			System.out.println("Enter date in this format: yyyy/MM/dd eg. 2021/02/01");
			Scanner keyboard = new Scanner(System.in);
			String simpleDateInput = keyboard.nextLine();
			if (simpleDateInput.length() == 10)
				return simpleDateInput;
			System.out.println("Expecting your entry to be 10 characters long.");
		} while (true);

	}

	public String checkSimpleEntryDateTime(String simpleEntryDateTime) {
		simpleEntryDateTime.trim();
		if (simpleEntryDateTime.length() != 16)
			System.out.println("String @ SimpleEntryDateTime is not 16 characters long");
		boolean isDayOk = false;
		String year;
		String month;
		String day;
		String hours;
		String mins;

		try {
			String checkString = "";
			for (int i = 0; i < simpleEntryDateTime.length(); i++) {
				if (simpleEntryDateTime.charAt(i) == '/')
					checkString = checkString + (i + 1);
			}

			if (checkString.equals("581114")) {
				int yearI = parseInt(simpleEntryDateTime.
				substring(0, simpleEntryDateTime.indexOf('/')));
				int monthI = parseInt(simpleEntryDateTime.
				substring(5, simpleEntryDateTime.indexOf('/', 6)));
				int dayI = parseInt(simpleEntryDateTime.
				substring(8, simpleEntryDateTime.indexOf('/', 9)));
				int hoursI = parseInt(simpleEntryDateTime.
				substring(11, simpleEntryDateTime.indexOf('/', 12)));
				int minsI = parseInt(simpleEntryDateTime.
				substring(14, 16));

				boolean isLeapYear = isLeapYear(yearI);
				if (monthI > 0 && monthI <= 12)
					if (hoursI >= 0 && hoursI <= 24)
						if (minsI >= 0 && minsI <= 59)
							isDayOk = isDayOk(monthI, dayI, isLeapYear);
				//inelegant solution to losing leading zeros:
				year = simpleEntryDateTime.
				substring(0, simpleEntryDateTime.indexOf('/'));
				month = simpleEntryDateTime.
				substring(5, simpleEntryDateTime.indexOf('/', 6));
				day = simpleEntryDateTime.
				substring(8, simpleEntryDateTime.indexOf('/', 9));
				hours = simpleEntryDateTime.
				substring(11, simpleEntryDateTime.indexOf('/', 12));
				mins = simpleEntryDateTime.
				substring(14, 16);
				if (isDayOk)
					return year + "-" + month + "-" + day + " " + hours + ":" + mins;
			}
		} catch (Exception ex) {
			System.out.println("Error: @SimpleEntryDateTime");
		}
		return null;
	}

	public String getUserSimpleDateTime() {
		do {
			System.out.println("Enter date time in this format: yyyy/MM/dd/hh/mm eg. 2021/02/01/00/45");
			Scanner keyboard = new Scanner(System.in);
			String simpleDateTimeInput = keyboard.nextLine();
			if (simpleDateTimeInput.length() == 16)
				return simpleDateTimeInput;
			System.out.println("Expecting your entry to be 16 characters long.");
		} while (true);

	}

	public String getUserString(String question) {
		return getUserString(question, false);
	}

	public String getUserString(String question, boolean allowBlank) {
		do {
			if (question != null)
				System.out.println(question);

			Scanner keyboard = new Scanner(System.in);
			String input = keyboard.nextLine().trim();

			if (allowBlank || !input.isBlank())
				return input;
			else
				System.out.println("Input Error: No blanks allowed, try again.");
		} while (true);
	}

	public String getUserNumberPlate(String question) {
		do {
			if (question != null)
				System.out.println(question);

			Scanner keyboard = new Scanner(System.in);
			String input = keyboard.nextLine().trim().toUpperCase();

			if (!input.isBlank() && input.length() < 12)
				return input;
			else
				System.out.println("Input Error:Input less than 12 chars and no blanks allowed, try again.");
		} while (true);
	}

	public boolean askUserYesNoQuestion(String question, boolean useBlankDefault, boolean blankDefaultReturn) {
		Scanner keyboard = new Scanner(System.in);
		String input;
		do {
			if (question != null)
				System.out.println(question);

			input = keyboard.nextLine().trim().toLowerCase();
			if (input.isBlank() && !useBlankDefault) {
				System.out.println("Input error: No Blanks allowed, try again");
				continue;
			}
			if (input.isBlank() && useBlankDefault) {
				return blankDefaultReturn;
			}

			switch (input) {
				case "min":
				case "y":
				case "yes":
				case "j":
				case "true":
					return true;
				case "full":
				case "n":
				case "no":
				case "false":
					return false;
				default:
					System.out.println("Input error: found not translate input to boolean, try again.");
			}
		} while (true);
	}

	public boolean askUserEnterOrFalse(String question) {
		Scanner keyboard = new Scanner(System.in);
		String input;
		if (question != null)
			System.out.print(question);

		input = keyboard.nextLine().trim().toLowerCase();

		return input.isBlank();
	}

	public boolean askUserEnterOrTrue(String question) {
		Scanner keyboard = new Scanner(System.in);
		String input;
		if (question != null)
			System.out.print(question);

		input = keyboard.nextLine().trim().toLowerCase();

		return !input.isBlank();
	}

	public  boolean askUserYesNoQuestion(String question) {
		return askUserYesNoQuestion(question, false, false);
	}

	public int askUserInputInteger(String question) {
		do {
			if (question != null && !question.isBlank())
				System.out.println(question);
			try {
				Scanner keyboard = new Scanner(System.in);
				return keyboard.nextInt();
			} catch (InputMismatchException ime) {
				System.out.println("Input error: Integer expected");
			}
		} while (true);
	}

	public int askUserInputInteger(String question, int minValue, int maxValue) {
		do {
			int number = askUserInputInteger(question);
			if ((number >= minValue) && (number <= maxValue))
				return number;
		} while (true);
	}
}