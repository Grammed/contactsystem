package uk.co.gram.logic;
import uk.co.gram.models.Address;
import uk.co.gram.models.Contact;
import uk.co.gram.models.PhoneType;
import uk.co.gram.repositories.ContactRepository;
import uk.co.gram.repositories.ContactRepositoryImp;
import uk.co.gram.tools.AdjustedConsoleInputTool;

import java.util.ArrayList;
import java.util.List;

public class ContactEditingLogic {
	AdjustedConsoleInputTool adj = new AdjustedConsoleInputTool();

public void chooseContactToEdit() {
	ContactRepository contactRepository = new ContactRepositoryImp();
	ArrayList<Integer> validChoices;
	Integer choice;
	// retrieve contact eventually via search
	boolean listContacts = adj.askUserEnterOrFalse("\nlist existing contacts -> enter\nsearch for contact by name" +
	" -> any character + enter");

	if(listContacts) {
		boolean invalidChoice = true;
		do {
			validChoices = printAllContacts(contactRepository.retrieveAllContacts());
			choice = adj.askUserInputInteger("enter Id of contact to edit ->");
			if (validChoices.contains(choice))
				invalidChoice = false;
			else
				System.out.println("Invalid contact Id, try again.");
		}while(invalidChoice);
		editContact(contactRepository.retrieveSingleContact(choice));
	} else {
		//search groups searchGroupsMethod() == main search reused.
		System.out.println("SearchGroups --> STUB");
	}
}

// todo create print class
private ArrayList<Integer> printAllContacts(List<Contact> contactList) {
	ArrayList<Integer> validChoices = new ArrayList<>();
	for(Contact c : contactList) {
		validChoices.add(c.getId());
		System.out.println(c.getId() + " " + c.getPhoneNumbers().get(PhoneType.HOME) + " " + c.getAddress().getCity() + " " + c.getFirstName() + " " + c.getLastName() + " " + c.getDob());
	}
	return validChoices;
}

private void editContact(Contact retrievedContact) {
	ContactRepository contactRepository = new ContactRepositoryImp();
	Integer choice;
	boolean yesEdit;

	do {
		System.out.println(retrievedContact);
		yesEdit = adj.askUserEnterOrFalse("\nedit this contact -> enter\nquit -> any character + enter");
		if (!yesEdit)
			break;
		choice = adj.askUserInputInteger("input number of info you want to edit 1-6 ->",1,6);
		contactRepository.editField(retrievedContact, choice);

	}while(yesEdit);

}
public List<String> editEmails(List<String> emails, AdjustedConsoleInputTool adj) {
	boolean ongoingEdit; // these would be confirmation popups on site.
	boolean ongoingDeletion;
	boolean editingEmails;
	do {
		for (int i = 0; i < emails.size(); i++) {
			System.out.println((i + 1) + "." + emails.get(i));
		}
		boolean edit = adj.askUserEnterOrFalse("\nedit email -> enter\nremove email -> any character + enter");
		int choice = (adj.askUserInputInteger("input number of email to edit/remove: ") -1);
		if(edit) {
			do {
				String currentValue = emails.get(choice);
				String update = adj.getUserString("Current email: " + currentValue + "\n    enter new email: ");
				ongoingEdit = adj.askUserEnterOrTrue(currentValue + " --> " + update +
				"\ncorrect -> enter\ntry again -> any character + enter");
				if(!ongoingEdit)
					emails.set(choice,update);
			}while(ongoingEdit);
		} else {
			do {
				String currentValue = emails.get(choice);
				ongoingDeletion = adj.askUserEnterOrFalse("\ncancel and keep " + currentValue + " -> enter" +
				"\ndelete " + currentValue +  " -> any character and enter");
				if(!ongoingDeletion)
					emails.remove(choice);
			}while(ongoingDeletion);
		}
		editingEmails = adj.askUserEnterOrFalse("\nedit/delete another -> enter" +
		"\nto previous menu -> any character + enter");

		if(editingEmails && emails.isEmpty()){
			System.out.println("No emails left to edit!");
			editingEmails = false;
		}
	} while (editingEmails);

	return emails;
}


public Address editAddress(Address address){
	return null;
}



}
