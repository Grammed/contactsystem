package uk.co.gram.logic;

import uk.co.gram.models.*;
import uk.co.gram.repositories.ContactGroupRepository;
import uk.co.gram.repositories.ContactGroupRepositoryImp;
import uk.co.gram.repositories.ContactRepositoryImp;
import uk.co.gram.tools.AdjustedConsoleInputTool;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ContactLogic {
AdjustedConsoleInputTool adjustedCIT = new AdjustedConsoleInputTool();
ContactRepositoryImp contactRepository = new ContactRepositoryImp();


public void collectNewContactData() {
	boolean addContactGroups;
	DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	Contact contact = new Contact();

	contact.setFirstName(adjustedCIT.getUserString("\nplease enter contacts first name -> ", false));
	contact.setLastName(adjustedCIT.getUserString("\nlast Name -> ", false));
	contact.setAddress(setContactAddress());
	System.out.println("\ndate of birth ->");
	contact.setDob(LocalDate.parse(adjustedCIT.getUserSimpleDate(), dateTimeFormatter));
	contact.setEmails(addEmails());
	contact.setPhoneNumbers(setNewContactNumbers());
	addContactGroups = adjustedCIT.askUserEnterOrFalse("\nadd this contact to a contact group -> enter \ncarry on -> any character + enter");
	if (addContactGroups)
		setContactGroups(contact);


	contactRepository.addOrUpdateContact(contact);
}


private void setContactGroups(Contact contact) {
	int choice;
	boolean addingGroups;
	ContactGroupRepository contactGroupRepository = new ContactGroupRepositoryImp();
	do {
		//todo print better
		System.out.println(contactGroupRepository.retrieveAllGroups());

		choice = adjustedCIT.askUserInputInteger("\nenter ID of group to add " +
		contact.getFirstName() + " " + contact.getLastName() + " to ->");

		//contact.getContactGroups().add(contactGroupRepository.retrieveSingleGroup(choice)); // this allowed duplicates.
		contactRepository.addGroupToContact(contact, contactGroupRepository.retrieveSingleGroup(choice));

		addingGroups = adjustedCIT.askUserEnterOrFalse("\nadd another -> enter\nmove on -> any character + enter");

	} while (addingGroups);
}


void setContactToNewGroup(ContactGroup contactGroup, Contact contact) {
	contact.getContactGroups().add(contactGroup);
	contactRepository.addOrUpdateContact(contact);
}


private Map<PhoneType, Phone> setNewContactNumbers() {
	int choice;
	PhoneType phoneType = null;
	boolean moreNumbers;
	boolean breakOut;
	Map<PhoneType, Phone> phoneMap = new HashMap<>();
	ArrayList<PhoneType> typesNotFilled = new ArrayList<>(3);
	typesNotFilled.add(PhoneType.HOME);
	typesNotFilled.add(PhoneType.WORK);
	typesNotFilled.add(PhoneType.OTHER);

	do {

		if (typesNotFilled.size() == 0) {
			breakOut = adjustedCIT.askUserEnterOrFalse("\none number of each type has been added " +
			"\nexit -> enter" +
			"\ncontinue (and overwrite already entered numbers) -> any character + enter");
			if (breakOut)
				break;
		}

		Phone phone = new Phone();
		phone.setNumber(adjustedCIT.getUserString("\nenter phone number ->", false));
		choice = adjustedCIT.askUserInputInteger("\n" + phone.getNumber() + " is -> \n1. home number\n" +
		"2. work number\n" +
		"3. other number\n" +
		"NOTE: only one of each type can be stored ->", 1, 3);
		switch (choice) {
			case 1:
				phoneType = PhoneType.HOME;
				typesNotFilled.removeIf(i -> (i == PhoneType.HOME));
				break;
			case 2:
				phoneType = PhoneType.WORK;
				typesNotFilled.removeIf(i -> (i == PhoneType.WORK));
				break;
			case 3:
				phoneType = PhoneType.OTHER;
				typesNotFilled.removeIf(i -> (i == PhoneType.OTHER));
				break;
			default:
				System.out.println("Something went wrong in phone switch.");
		}
		phoneMap.put(phoneType, phone);
		moreNumbers = adjustedCIT.askUserEnterOrFalse("\nadd another number -> enter\nexit -> any character + enter");


	} while (moreNumbers);

	return phoneMap;
}


public Map<PhoneType, Phone> setNewContactNumbers(Map<PhoneType, Phone> retrievedPhoneMap) {
	ArrayList<PhoneType> alreadyFilled = new ArrayList<>(3);
	if(!retrievedPhoneMap.isEmpty()) {
		System.out.println("CURRENT NUMBERS:");
		retrievedPhoneMap.forEach((key, value) -> {
			System.out.println(key + " " + value);
			alreadyFilled.add(key);
		});
	}

	int choice;
	PhoneType phoneType = null;
	boolean moreNumbers;
	boolean breakOut;
	ArrayList<PhoneType> typesNotFilled = new ArrayList<>(3);
	Map<PhoneType, Phone> phoneMap = retrievedPhoneMap;
	typesNotFilled.add(PhoneType.HOME);
	typesNotFilled.add(PhoneType.WORK);
	typesNotFilled.add(PhoneType.OTHER);

	typesNotFilled.removeAll(alreadyFilled);

	do {

		if (typesNotFilled.size() == 0) {
			breakOut = adjustedCIT.askUserEnterOrFalse("\none number of each type has been added " +
			"\nexit -> enter" +
			"\ncontinue (and overwrite already entered numbers) -> any character + enter");
			if (breakOut)
				break;
		}

		Phone phone = new Phone();
		phone.setNumber(adjustedCIT.getUserString("\nenter phone number ->", false));
		choice = adjustedCIT.askUserInputInteger("\n" + phone.getNumber() + " is -> \n1. home number\n" +
		"2. work number\n" +
		"3. other number\n" +
		"NOTE: only one of each type can be stored ->", 1, 3);
		switch (choice) {
			case 1:
				phoneType = PhoneType.HOME;
				typesNotFilled.removeIf(i -> (i == PhoneType.HOME));
				break;
			case 2:
				phoneType = PhoneType.WORK;
				typesNotFilled.removeIf(i -> (i == PhoneType.WORK));
				break;
			case 3:
				phoneType = PhoneType.OTHER;
				typesNotFilled.removeIf(i -> (i == PhoneType.OTHER));
				break;
			default:
				System.out.println("Something went wrong in phone switch.");
		}
		retrievedPhoneMap.put(phoneType, phone);
		moreNumbers = adjustedCIT.askUserEnterOrFalse("\nadd another number -> enter\nexit -> any character + enter");


	} while (moreNumbers);

	return retrievedPhoneMap;
}


public List<String> addEmails() {
	boolean moreEmails;
	List<String> emailList = new ArrayList<>();
	do {
		emailList.add(adjustedCIT.getUserString("enter email: ", false));

		moreEmails = adjustedCIT.askUserEnterOrFalse("\nadditional email -> enter " +
		"\nthat's all of them -> any character + enter");

	} while (moreEmails);

	return emailList;
}


public Address setContactAddress() {
	Address address = new Address();
	boolean notOk;

	do {

		address.setStreet(adjustedCIT.getUserString("\nenter street name -> ", false));
		address.setNumber(adjustedCIT.getUserString("street number -> ", false));
		address.setZipCode(adjustedCIT.getUserString("zip -> ", false));
		address.setCity(adjustedCIT.getUserString("city -> ", false));
		address.setCountry(adjustedCIT.getUserString("country -> ", true));

		System.out.println(address);

		notOk = adjustedCIT.askUserEnterOrTrue("\ncontact address ok -> enter" +
		"\ntry again -> any character + enter");

	}while(notOk);

	return address;
}


public void viewAndDeleteContacts() throws Exception {
	boolean viewDelete;
	int choice;

	do {
		printAllContacts(contactRepository.retrieveAllContacts());
		viewDelete = adjustedCIT.askUserEnterOrFalse("delete a contact -> enter" +
		"\nmove on -> any character + enter");
		if (viewDelete) {
			choice = adjustedCIT.askUserInputInteger("enter id of contact to delete -> ");
			contactRepository.deleteSingleContact(choice);

		}
	} while (viewDelete);
}




public void addContactToGroup() {
	boolean addingContactsToGroups;
	int choice;

	do {
		printAllContacts(contactRepository.retrieveAllContacts());
		ContactGroupRepository contactGroupRepository = new ContactGroupRepositoryImp();
		choice = adjustedCIT.askUserInputInteger("Enter the id of the contact you want add to group: ");
		Contact contact = contactRepository.retrieveSingleContact(choice);
		List<ContactGroup> contactGroupList = contact.getContactGroups();
		if (contactGroupList.size() > 0) {
			printCurrentContactGroups("This contact is currently in these groups: ", contactGroupList);
		} else {
			System.out.println("Contact does not belong to any groups");
		}
		List<ContactGroup> allContactGroups = contactGroupRepository.retrieveAllGroups();
		printAllContactGroups(allContactGroups);
		choice = adjustedCIT.askUserInputInteger("Which group do you want to add " + contact.getFirstName() + " " + contact.getLastName() + " to?");
		ContactGroup contactGroup = contactGroupRepository.retrieveSingleGroup(choice);
		contactRepository.addGroupToContact(contact, contactGroup);

		addingContactsToGroups = adjustedCIT.askUserEnterOrFalse("Carry on adding contacts to groups? -> enter" +
		"\nBack to previous menu -> any character and enter");


	} while (addingContactsToGroups);
}


public void removeContactFromGroup() {
	boolean removingContactsFromGroup = true;
	boolean exit;
	int choice;

	do {
		printAllContacts(contactRepository.retrieveAllContacts());
		ContactGroupRepository contactGroupRepository = new ContactGroupRepositoryImp();
		choice = adjustedCIT.askUserInputInteger("enter the id of the contact you want to remove from a group -> ");
		Contact contact = contactRepository.retrieveSingleContact(choice);
		List<ContactGroup> contactGroupList = contact.getContactGroups();

		if (contactGroupList.size() > 0) {
			printCurrentContactGroups("\ncontact is currently in these groups: ", contactGroupList);
		} else {
			exit = adjustedCIT.askUserEnterOrFalse("\ncontact is not in any groups -> enter to exit\nview more contacts -> any character + enter");
			if (exit)
				break;
			else
				continue;
		}

		choice = adjustedCIT.askUserInputInteger("\ninput group you want to remove" + contact.getFirstName() + " " + contact.getLastName() + " from ->");
		ContactGroup contactGroup = contactGroupRepository.retrieveSingleGroup(choice);
		contactRepository.removeGroupFromContact(contact, contactGroup);

		removingContactsFromGroup = adjustedCIT.askUserEnterOrFalse("\ncontinue removing contacts from groups -> enter" +
		"\nback to previous menu -> any character + enter");


	} while (removingContactsFromGroup);
}

// todo this is used in chooseContactToEdit
private ArrayList<Integer> printAllContacts(List<Contact> contactList) {
	ArrayList<Integer> validChoices = new ArrayList<>();
	for (Contact c : contactList) {
		validChoices.add(c.getId());
		System.out.println(c.getId() + " " + c.getPhoneNumbers().get(PhoneType.HOME) + " " + c.getAddress().getCity() + " " + c.getFirstName() + " " + c.getLastName() + " " + c.getDob());
	}
	return validChoices;
}


private void printCurrentContactGroups(String s, List<ContactGroup> currentGroups) {
	if (s != null) {
		System.out.println(s);
	}
	for (ContactGroup cg : currentGroups)
		System.out.println(cg.getId() + " " + cg.getName() + " " + cg.getStartDate());
}


private void printAllContactGroups(List<ContactGroup> allGroups) {
	for (ContactGroup cg : allGroups)
		System.out.println(cg.getId() + " " + cg.getName() + " " + cg.getStartDate());
}

}
