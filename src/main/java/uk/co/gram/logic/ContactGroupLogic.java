package uk.co.gram.logic;

import uk.co.gram.models.Contact;
import uk.co.gram.models.ContactGroup;
import uk.co.gram.repositories.ContactGroupRepository;
import uk.co.gram.repositories.ContactGroupRepositoryImp;
import uk.co.gram.repositories.ContactRepository;
import uk.co.gram.repositories.ContactRepositoryImp;
import uk.co.gram.tools.AdjustedConsoleInputTool;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ContactGroupLogic {
	AdjustedConsoleInputTool adjustedCIT = new AdjustedConsoleInputTool();
	ContactGroupRepository contactGroupRepository = new ContactGroupRepositoryImp();

public void collectNewGroupData() {
	boolean today;
	boolean addContacts;
	DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	ContactGroup contactGroup = new ContactGroup();

	contactGroup.setName(adjustedCIT.getUserString("Please enter groups name: ", false));
	today = adjustedCIT.askUserEnterOrFalse("Today is this group's start-date? press enter, " +
	"\nAny other character and enter to input another date.");

	if (today)
		contactGroup.setStartDate(LocalDate.now());
	else {
		System.out.println("Groups start date.");
		contactGroup.setStartDate(LocalDate.parse(adjustedCIT.getUserSimpleDate(), dateTimeFormatter));
	}

	contactGroup.setDescription(adjustedCIT.getUserString("Enter group description: ", false));

	contactGroupRepository.addNewGroup(contactGroup);
	System.out.println("New group has " + contactGroup + " has been created");
	addContacts = adjustedCIT.askUserEnterOrFalse("add contacts to it?\nenter --> yes\nany character & enter --> no");
	if (addContacts)
		setContact(contactGroup);

}

public void chooseGroupToEdit() {
	ArrayList<Integer> validChoices;
	Integer choice;
	// retrieve group eventually via search
	boolean listGroups = adjustedCIT.askUserEnterOrFalse("\nlist existing groups -> enter\nsearch for group by name" +
	" -> any character + enter");

	if(listGroups) {
		boolean invalidChoice = true;
		do {
			validChoices = printAllContactGroups(contactGroupRepository.retrieveAllGroups());
			choice = adjustedCIT.askUserInputInteger("enter Id of group to edit ->");
			if (validChoices.contains(choice))
				invalidChoice = false;
			else
				System.out.println("Invalid group Id, try again.");
		}while(invalidChoice);
		editGroup(contactGroupRepository.retrieveSingleGroup(choice));
		} else {
		//search groups searchGroupsMethod() == main search reused.
		System.out.println("SearchGroups --> STUB");
	}
	}

private void editGroup(ContactGroup retrievedGroup) {
	Integer choice;
	boolean yesEdit;

	do {
		System.out.println(retrievedGroup);
		yesEdit = adjustedCIT.askUserEnterOrFalse("\nedit this group -> enter\nquit -> any character + enter");
		if (!yesEdit)
			break;
		choice = adjustedCIT.askUserInputInteger("1-3 enter number corresponding to the info you want to edit ->");
		contactGroupRepository.editField(retrievedGroup, choice);

	}while(yesEdit);

}



private void setContact(ContactGroup contactGroup) {
	int choice;
	boolean addingContacts;
	Contact contact;
	ContactLogic contactLogic = new ContactLogic();
	ContactRepository contactRepository = new ContactRepositoryImp();
	do {
		System.out.println(contactRepository.retrieveAllContacts());

		choice = adjustedCIT.askUserInputInteger("Which contact would you like to add to" +
		contactGroup.getName()  + "?\nEnter Id: ");

		contact = contactRepository.retrieveSingleContact(choice);
		contactLogic.setContactToNewGroup(contactGroup, contact);

		addingContacts = adjustedCIT.askUserEnterOrFalse("add another?\npress enter\nor any character and enter to move on.");

	}while(addingContacts);


}


public void viewAndDeleteContactGroups() {
	boolean viewDelete;
	int choice;

	do {
		printAllContactGroups(contactGroupRepository.retrieveAllGroups());
		viewDelete = adjustedCIT.askUserEnterOrFalse("\ndelete contact group -> enter" +
		"\nmove on -> any character + enter");
		if(viewDelete) {
			choice =	adjustedCIT.askUserInputInteger("Enter id of contact group to delete: ");
			contactGroupRepository.deleteSingleContactGroup(choice);
		}

	}while(viewDelete);
}

private ArrayList<Integer> printAllContactGroups(List<ContactGroup> contactGroupList) {
	ArrayList<Integer> validChoices = new ArrayList<>();
	System.out.println("CONTACT GROUPS:");
	for(ContactGroup cg : contactGroupList) {
		validChoices.add(cg.getId());
		System.out.println(cg.getId() + " " + cg.getName() + " " + cg.getStartDate() + " " + cg.getDescription());
	}
	return validChoices;
}

private void printCGroupsAndContacts(List<ContactGroup> contactGroupList) {
	System.out.println("CONTACT GROUPS:");
	for(ContactGroup cg : contactGroupList) {
		System.out.println("\nId--> " + cg.getId() + "   " + cg.getName() + " " + cg.getStartDate() + " " + cg.getDescription());
		for (Contact c : cg.getContacts())
			System.out.println(c); // todo format to strings.
	}
}

private void printCGroupAndContacts(ContactGroup cg) {
	System.out.println("CONTACT GROUPS " + cg.getName());
		System.out.println("\nId--> " + cg.getId() + "   " + cg.getName() + " " + cg.getStartDate() + " " + cg.getDescription());
		for (Contact c : cg.getContacts())
			System.out.println(c); // todo format to strings.
	}
}

